"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pricecode_1 = require("./pricecode");
var Movie = (function () {
    function Movie(title, priceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }
    Movie.prototype.points = function (daysRented) {
        // add bonus for a two day new release rental
        if (this.priceCode == pricecode_1.PriceCode.NEW_RELEASE) {
            if (daysRented > 1) {
                return 2;
            }
        }
        return 1;
    };
    Movie.prototype.cost = function (daysRented) {
        var cost = 0;
        switch (this.priceCode) {
            case pricecode_1.PriceCode.REGULAR:
                cost += 2;
                if (daysRented > 2)
                    cost += (daysRented - 2) * 1.5;
                break;
            case pricecode_1.PriceCode.NEW_RELEASE:
                cost += daysRented * 3;
                break;
            case pricecode_1.PriceCode.CHILDRENS:
                cost += 1.5;
                if (daysRented > 3)
                    cost += (daysRented - 3) * 1.5;
                break;
        }
        return cost;
    };
    return Movie;
}());
exports.Movie = Movie;
//# sourceMappingURL=movie.js.map