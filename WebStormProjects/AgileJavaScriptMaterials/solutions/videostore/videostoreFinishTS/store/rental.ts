import { Movie } from './movie';

export class Rental {
    constructor(public movie: Movie, public daysRented: number) {
    }

    points(): number {
        return this.movie.points(this.daysRented);
    }

    cost(): number {
        return this.movie.cost(this.daysRented);
    }
}
