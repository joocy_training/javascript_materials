"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pricecode_1 = require("./pricecode");
var movie_1 = require("./movie");
var rental_1 = require("./rental");
var customer_1 = require("./customer");
function runDemo() {
    var peterPan = new movie_1.Movie("Peter Pan", pricecode_1.PriceCode.CHILDRENS);
    var theHulk = new movie_1.Movie("The Hulk", pricecode_1.PriceCode.REGULAR);
    var starWars = new movie_1.Movie("Star Wars", pricecode_1.PriceCode.REGULAR);
    var toyStory = new movie_1.Movie("Toy Story", pricecode_1.PriceCode.CHILDRENS);
    var killBill = new movie_1.Movie("Kill Bill", pricecode_1.PriceCode.NEW_RELEASE);
    var r1 = new rental_1.Rental(peterPan, 2);
    var r2 = new rental_1.Rental(theHulk, 1);
    var r3 = new rental_1.Rental(starWars, 3);
    var r4 = new rental_1.Rental(toyStory, 2);
    var r5 = new rental_1.Rental(killBill, 4);
    var customer = new customer_1.Customer("Joe Bloggs");
    customer.addRental(r1);
    customer.addRental(r2);
    customer.addRental(r3);
    customer.addRental(r4);
    customer.addRental(r5);
    var domNode = document.getElementById("statement");
    domNode.innerText = customer.statement();
}
exports.runDemo = runDemo;
//# sourceMappingURL=main.js.map