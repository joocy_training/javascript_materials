"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Customer = (function () {
    function Customer(name) {
        this.name = name;
        this.rentals = new Array();
    }
    Customer.prototype.addRental = function (rental) {
        this.rentals.push(rental);
    };
    Customer.prototype.totalCost = function () {
        var totalAmount = 0;
        for (var _i = 0, _a = this.rentals; _i < _a.length; _i++) {
            var rental = _a[_i];
            totalAmount += rental.cost();
        }
        return totalAmount;
    };
    Customer.prototype.totalPoints = function () {
        var allPoints = this.rentals.map(function (r) { return r.points(); });
        //this should be a reduction e.g. using lodash
        var points = 0;
        allPoints.forEach(function (p) { return points += p; });
        return points;
    };
    Customer.prototype.statement = function () {
        var result = "";
        // add header lines
        result += "\nRental Record for " + this.name + "\n";
        for (var _i = 0, _a = this.rentals; _i < _a.length; _i++) {
            var r = _a[_i];
            // show figures for this rental
            result += "\t" + r.daysRented + "\t" + r.movie.title + "\t" + r.cost() + "\n";
        }
        // add footer lines
        result += "Amount owed is " + this.totalCost() + "\n";
        result += "You earned " + this.totalPoints() + " frequent renter points\n";
        return result;
    };
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.js.map