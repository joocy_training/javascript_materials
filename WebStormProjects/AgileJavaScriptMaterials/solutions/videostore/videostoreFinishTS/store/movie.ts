import { PriceCode } from "./pricecode"

export class Movie {
    constructor(public title: string, public priceCode: PriceCode) {
    }

    points(daysRented: number): number {
        // add bonus for a two day new release rental
        if (this.priceCode == PriceCode.NEW_RELEASE) {
            if (daysRented > 1) {
                return 2;
            }
        }
        return 1;
    }

    cost(daysRented): number {
        var cost = 0;
        switch (this.priceCode) {
            case PriceCode.REGULAR:
                cost += 2;
                if (daysRented > 2)
                    cost += (daysRented - 2) * 1.5;
                break;
            case PriceCode.NEW_RELEASE:
                cost += daysRented * 3;
                break;
            case PriceCode.CHILDRENS:
                cost += 1.5;
                if (daysRented > 3)
                    cost += (daysRented - 3) * 1.5;
                break;
        }
        return cost;
    }
}
