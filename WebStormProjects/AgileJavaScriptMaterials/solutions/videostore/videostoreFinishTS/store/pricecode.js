"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PriceCode;
(function (PriceCode) {
    PriceCode[PriceCode["REGULAR"] = 0] = "REGULAR";
    PriceCode[PriceCode["NEW_RELEASE"] = 1] = "NEW_RELEASE";
    PriceCode[PriceCode["CHILDRENS"] = 2] = "CHILDRENS";
})(PriceCode = exports.PriceCode || (exports.PriceCode = {}));
//# sourceMappingURL=pricecode.js.map