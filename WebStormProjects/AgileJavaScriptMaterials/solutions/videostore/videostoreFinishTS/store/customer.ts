import { Rental } from './rental'

export class Customer {
    rentals: Array<Rental>;
    constructor(public name: string) {
        this.rentals = new Array<Rental>();
    }

    addRental(rental): void {
        this.rentals.push(rental);
    }

    totalCost(): number {
        var totalAmount = 0;
        for (let rental of this.rentals) {
            totalAmount += rental.cost();
        }
        return totalAmount;
    }

    totalPoints(): number {
        let allPoints = this.rentals.map(r => r.points());

        //this should be a reduction e.g. using lodash
        let points = 0;
        allPoints.forEach(p => points += p);
        return points;
    }

    statement(): string {
        var result = "";
        // add header lines
        result += `\nRental Record for ${this.name}\n`;
        for (let r of this.rentals) {
            // show figures for this rental
            result += `\t${r.daysRented}\t${r.movie.title}\t${r.cost()}\n`;
        }
        // add footer lines
        result += `Amount owed is ${this.totalCost()}\n`;
        result += `You earned ${this.totalPoints()} frequent renter points\n`;

        return result;
    }
}
