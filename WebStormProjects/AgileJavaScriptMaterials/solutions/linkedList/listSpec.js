describe("List Tests", function() {
    var list;
    function addFiveItems() {
        list.add("abc");
        list.add("def");
        list.add("ghi");
        list.add("jkl");
        list.add("mno");
    }
    beforeEach(function() {
        list = new List();
    });
    it("New list is empty", function() {
        expect(list.isEmpty()).toBeTruthy();
        expect(list.size()).toEqual(0);
    });
    it("List with content has size", function() {
        addFiveItems();
        expect(list.isEmpty()).toBeFalsy();
        expect(list.size()).toEqual(5);
    });
    it("First item can be retrieved", function() {
        list.add("abc");
        expect(list.get(0)).toEqual("abc");
    });
    it("Many items can be retrieved", function() {
        addFiveItems();
        expect(list.get(0)).toEqual("abc");
        expect(list.get(1)).toEqual("def");
        expect(list.get(2)).toEqual("ghi");
        expect(list.get(3)).toEqual("jkl");
        expect(list.get(4)).toEqual("mno");
    });
});
