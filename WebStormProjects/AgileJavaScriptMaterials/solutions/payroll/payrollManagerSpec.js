describe("Payroll Manager Tests", function () {
    var manager;
    var repository;
    function enrollFourEmployees() {
        manager.enroll("Dave",27,"HR",24000.0);
        manager.enroll("Jane",29,"IT",36000.0);
        manager.enroll("Pete",31,"HR",48000.0);
        manager.enroll("Mary",33,"IT",60000.0);
    }
    beforeEach(function () {
        repository = new Repository();
        manager = new PayrollManager(repository);
    });
    it("Employees can be enrolled", function() {
        spyOn(repository, 'add').and.returnValue("AB12");
        var id = manager.enroll("Dave",27,"HR",24000.0);

        expect(id).toEqual("AB12");
        var emp = new Employee("Dave",27,"HR",24000.0);
        expect(repository.add).toHaveBeenCalledWith(emp);
    });
    it("Company payroll can be calculated", function() {
        var data = {};
        function fakeAdd(emp) {
            data[emp.getName()] = emp;
            return emp.getName();
        }
        function fakeFind(id) {
            return data[id];
        }
        spyOn(repository, 'add').and.callFake(fakeAdd);
        spyOn(repository, 'find').and.callFake(fakeFind);
        enrollFourEmployees();

        var retval = manager.runPayrollForCompany();
        expect(retval).toEqual(14000.0);
        expect(repository.add.calls.count()).toEqual(4);
        expect(repository.find.calls.count()).toEqual(4);
    });
    it("Dept payroll can be calculated", function() {
        var data = {};
        function fakeAdd(emp) {
            data[emp.getName()] = emp;
            return emp.getName();
        }
        function fakeFind(id) {
            return data[id];
        }
        spyOn(repository, 'add').and.callFake(fakeAdd);
        spyOn(repository, 'find').and.callFake(fakeFind);
        enrollFourEmployees();

        var retval = manager.runPayrollForDept("IT");
        expect(retval).toEqual(8000.0);
        expect(repository.add.calls.count()).toEqual(4);
        expect(repository.find.calls.count()).toEqual(4);
    });
    it("Salaries can be incremented by dept", function() {
        var data = {};
        function fakeAdd(emp) {
            data[emp.getName()] = emp;
            return emp.getName();
        }
        function fakeFind(id) {
            return data[id];
        }
        spyOn(repository, 'add').and.callFake(fakeAdd);
        spyOn(repository, 'find').and.callFake(fakeFind);
        enrollFourEmployees();

        expect(manager.runPayrollForDept("IT")).toEqual(8000.0);
        manager.adjustSalaryByDept("IT",1200);
        expect(manager.runPayrollForDept("IT")).toEqual(8200.0);
        expect(repository.add.calls.count()).toEqual(4);
        expect(repository.find.calls.count()).toEqual(12);
    });
});
