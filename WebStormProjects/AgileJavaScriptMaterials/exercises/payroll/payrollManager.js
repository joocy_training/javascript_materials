function Employee(name,age,dept,salary) {
    this._name = name;
    this._age = age;
    this._dept = dept;
    this._salary = salary;
}
Employee.prototype.getName = function() { return this._name; };
Employee.prototype.getAge = function() { return this._age; };
Employee.prototype.getDept = function() { return this._dept; };
Employee.prototype.getSalary = function() { return this._salary; };
Employee.prototype.setSalary = function(amount) { this._salary = amount; };

function Repository() {
    this.add = function() {};
    this.find = function() {};
}

function PayrollManager(repository) {
    this._empRepository = repository;
    this._empIds = [];
}
PayrollManager.prototype.enroll = function(name,age,dept,salary) {
    var id = this._empRepository.add(new Employee(name,age,dept,salary));
    this._empIds.push(id);
    return id;
};
PayrollManager.prototype.runPayrollForCompany = function() {
    var amount = 0;
    for(var x=0;x<this._empIds.length;x++) {
        var id = this._empIds[x];
        var emp = this._empRepository.find(id);
        amount += emp.getSalary() / 12;
    }
    return amount;
};
PayrollManager.prototype.runPayrollForDept = function(dept) {
    var amount = 0;
    for(var x=0;x<this._empIds.length;x++) {
        var id = this._empIds[x];
        var emp = this._empRepository.find(id);
        if(emp.getDept() === dept) {
            amount += emp.getSalary() / 12;
        }
    }
    return amount;
};
PayrollManager.prototype.adjustSalaryByDept = function(dept, addition) {
    for(var x=0;x<this._empIds.length;x++) {
        var id = this._empIds[x];
        var emp = this._empRepository.find(id);
        if(emp.getDept() === dept) {
            var newSalary = emp.getSalary() + addition;
            emp.setSalary(newSalary);
        }
    }
};

