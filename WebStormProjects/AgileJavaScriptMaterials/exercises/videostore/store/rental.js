"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function Rental(movie, daysRented) {
    this.movie = movie;
    this.daysRented = daysRented;
}
exports.Rental = Rental;
//# sourceMappingURL=rental.js.map