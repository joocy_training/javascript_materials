function Node(next,previous,item) {
    this._next = next;
    this._previous = previous;
    this._item = item;
}
Node.prototype.getNext = function() { return this._next; };
Node.prototype.setNext = function(node) { this._next = node; };
Node.prototype.getPrevious = function() { return this._previous; };
Node.prototype.setPrevious = function(node) { this._previous = node; };
Node.prototype.getItem = function() { return this._item; };

function List() {
    this._size = 0;
    this._first = null;
}
List.prototype.isEmpty = function() { return this._size === 0; };
List.prototype.size = function() { return this._size; };
List.prototype.add = function(item) {
    if(this.isEmpty()) {
        this._first = new Node(null,null,item);
    }
    this._size++;
};
List.prototype.get = function(index) {
    if(index === 0) {
        return this._first.getItem();
    }
    return "";
};