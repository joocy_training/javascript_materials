const path = require("path");
const fs = require("fs");
const textPath = path.join(__dirname, 'books', 'PrideAndPrejudice.txt');

console.log(`Reading from '${textPath}'....`);
const fileContents = fs.readFileSync(textPath, 'utf8');

console.log('done');

// const processor = new BigramProcessor();
// const bigrams = processor.extractFromStringWithCount(fileContents);
// console.log(bigrams.size);
//
// let count = 1;
// for (let entry of bigrams) {
//     console.log(entry);
//     if (++count > 5) {
//         break;
//     }
// }

