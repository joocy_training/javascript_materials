import {Rental} from './rental'

export class Customer {
    constructor(name) {
        this.name = name;
        this.rentals = [];
    }

    addRental(rental) {
        this.rentals.push(rental);
    }

    addRentals(...rentals) {
        rentals.forEach(x => this.addRental(x));
    }

    totalCost() {
        let totalAmount = 0;
        for (let rental of this.rentals) {
            totalAmount += rental.cost;
        }
        return totalAmount;
    }

    totalPoints() {
        let allPoints = this.rentals.map(r => r.points);

        //this should be a reduction e.g. using lodash
        let points = 0;
        allPoints.forEach(p => points += p);
        return points;
    }

    statement() {
        let result = this.getHeader();
        this.rentals.forEach(x => result += this.getStatementRow(x));
        result += `Amount owed is ${this.totalCost()}\n`;
        result += `You earned ${this.totalPoints()} frequent renter points\n`;

        return result;
    }

    getStatementRow({daysRented, movie, cost}) {
        return `\t${daysRented}\t${movie.title}\t${cost}\n`;
    }

    getHeader() {
        return `\nRental Record for ${this.name}\n`;
    }
}
