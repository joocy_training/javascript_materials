import { Movie } from './movie';

export class Rental {
    constructor(movie, daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    get points() {
        return this.movie.points(this.daysRented);
    }

    get cost() {
        return this.movie.cost(this.daysRented);
    }
}
