import {pairwise, removePunctuation, uniquePairwise, uniquePairwiseCount} from "./utility";

describe("Utility functions", () => {
    describe("removePunctuation", () => {
        it("Handles the empty string case", () => {
           expect(removePunctuation("")).toEqual("");
        });

        it("Leaves a with no punctuation untouched", () => {
            const input = "This is a string with no punctuation";

            expect(removePunctuation(input)).toEqual(input);
        });

        it("Strips off simple punctuation", () => {
            const input = "This is a string with punctuation.";
            const expectedResult = "This is a string with punctuation";

            expect(removePunctuation(input)).toEqual(expectedResult);
        });

        it("Strips off only punctuation", () => {
            const input = "-;:@'#~][{}.,<>/?\|!£$%^&*()\"";

            expect(removePunctuation(input)).toEqual("");
        });
    });

    describe("pairwise", () => {
        it("Handles the empty array case", () => {
            expect(pairwise([])).toEqual([]);
        });

        it("Handles array with single element", () => {
            expect(pairwise(['A'])).toEqual([]);
        });

        it("Handles array with two elements", () => {
            expect(pairwise(['A', 'B'])).toEqual([['A', 'B']]);
        });

        it("Handles array with three elements", () => {
            expect(pairwise(['A', 'B', 'C'])).toEqual([['A', 'B'], ['B', 'C']]);
        });

        it("Handles array with many elements", () => {
            expect(pairwise(['A', 'B', 'C', 'D', 'E', 'F']))
                .toEqual([
                    ['A', 'B'],
                    ['B', 'C'],
                    ['C', 'D'],
                    ['D', 'E'],
                    ['E', 'F']
                ]);
        });
    });

    describe("uniquePairwise", () => {
        it("Handles the empty array case", () => {
            expect(uniquePairwise([])).toEqual([]);
        });

        it("Handles array with single element", () => {
            expect(uniquePairwise(['A'])).toEqual([]);
        });

        it("Handles array with two elements", () => {
            expect(uniquePairwise(['A', 'B'])).toEqual([['A', 'B']]);
        });

        it("Handles array with three elements", () => {
            expect(uniquePairwise(['A', 'B', 'C'])).toEqual([['A', 'B'], ['B', 'C']]);
        });

        it("Handles duplicates", () => {
            expect(uniquePairwise(['A', 'B', 'A', 'B'])).toEqual([['A', 'B'], ['B', 'A']]);
        });

        it("Handles array with many elements", () => {
            expect(uniquePairwise(['A', 'B', 'A', 'B', 'A', 'B']))
                .toEqual([
                    ['A', 'B'],
                    ['B', 'A']
                ]);
        });
    });

    describe("uniquePairwiseCount", () => {
        it("Handles the empty array case", () => {
            expect(uniquePairwiseCount([])).toEqual(new Map());
        });

        it("Handles array with single element", () => {
            expect(uniquePairwiseCount(['A'])).toEqual(new Map());
        });

        it("Handles array with two elements", () => {
            expect(uniquePairwiseCount(['A', 'B'])).toEqual(new Map([['A B', 1]]));
        });

        it("Handles array with three elements", () => {
            expect(uniquePairwiseCount(['A', 'B', 'C']))
                .toEqual(new Map([
                    ['A B', 1],
                    ['B C', 1]
                ]));
        });

        it("Handles duplicates", () => {
            expect(uniquePairwiseCount(['A', 'B', 'A', 'B']))
                .toEqual(new Map([
                    ['A B', 2],
                    ['B A', 1]
                ]));
        });

        it("Handles array with many elements", () => {
            expect(uniquePairwiseCount(['A', 'B', 'A', 'B', 'A', 'B']))
                .toEqual(new Map([
                    ['A B', 3],
                    ['B A', 2]
                ]));
        });
    });
});