import {pairwise, removePunctuation, uniquePairwise, uniquePairwiseCount} from "./utility";

export class BigramProcessor {
    extractFromString(text) {
        const filtered = removePunctuation(text.toLowerCase());
        const words = filtered.split(/[\s]+/);
        return uniquePairwise(words);
    }

    extractFromStringWithCount(text) {
        const filtered = removePunctuation(text.toLowerCase());
        const words = filtered.split(/[\s]+/);
        const bigramMap = uniquePairwiseCount(words);
        return [...bigramMap.entries()].sort((a, b) => b[1] - a[1])
    }
}