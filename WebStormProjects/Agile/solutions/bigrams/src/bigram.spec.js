import {BigramProcessor} from "./bigram";

describe("Bigram Processor", () => {
   const target = new BigramProcessor();

    describe("Process small strings with unique bigrams", () => {
        const inputText = "This is a small test";

        it("Extracts correct number of bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams.length).toEqual(4);
        });

        it("Extracts bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams).toEqual([
                ["this", "is"],
                ["is", "a"],
                ["a", "small"],
                ["small", "test"],
            ]);
        });
    });

    describe("Handles Punctation with unique bigrams", () => {
        const inputText = "This is a small test, but with some punctuation.";

        it("Extracts correct number of bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams.length).toEqual(8);
        });

        it("Extracts unique bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams).toEqual([
                ["this", "is"],
                ["is", "a"],
                ["a", "small"],
                ["small", "test"],
                ["test", "but"],
                ["but", "with"],
                ["with", "some"],
                ["some", "punctuation"],
            ]);
        });
    });

    describe("Handles non-unique bigrams - only reports set", () => {
        const inputText = "This is a small test. This is a repeated bigram example.";

        it("Extracts correct number of bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams.length).toEqual(8);
        });

        it("Extracts unique bigrams", () => {
            const bigrams = target.extractFromString(inputText);

            expect(bigrams).toEqual([
                ["this", "is"],
                ["is", "a"],
                ["a", "small"],
                ["small", "test"],
                ["test", "this"],
                ["a", "repeated"],
                ["repeated", "bigram"],
                ["bigram", "example"],
            ]);
        });
    });

    describe("Handles non-unique bigrams - reports sorted count", () => {
        const inputText = "This is a small test. This is a repeated bigram example.";

        it("Extracts correct number of bigrams", () => {
            const bigrams = target.extractFromStringWithCount(inputText);

            expect(bigrams.length).toEqual(8);
        });

        it("Extracts unique bigrams", () => {
            const bigrams = target.extractFromStringWithCount(inputText);

            expect(bigrams).toEqual([
                ["this is", 2],
                ["is a", 2],
                ["a small", 1],
                ["small test", 1],
                ["test this", 1],
                ["a repeated", 1],
                ["repeated bigram", 1],
                ["bigram example", 1],
            ]);
        });
    });
});