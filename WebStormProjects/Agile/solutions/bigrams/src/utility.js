export function removePunctuation(text) {
    return text.replace(/[^\w\s]/ig, "");
}

function* pairwiseGenerator(input) {
    for (let ii = 0; ii < input.length - 1; ii++) {
        yield [input[ii], input[ii + 1]];
    }
}

export function pairwise(input) {
    return [...pairwiseGenerator(input)];
}

let pairsEqual = function (pair1, pair2) {
    return pair1[0] === pair2[0] && pair1[1] === pair2[1];
};

export function uniquePairwise(input) {
    const result = [];
    for (let pair of pairwiseGenerator(input)) {
        if (!result.find(x => pairsEqual(pair, x))) {
            result.push(pair);
        }
    }

    return result;
}

export function uniquePairwiseCount(input) {
    const result = new Map();

    for (let [first, second] of pairwiseGenerator(input)) {
        const key = first + " " + second;
        const currentCount = result.get(key) || 0;
        result.set(key, currentCount + 1);
    }

    return result;
}