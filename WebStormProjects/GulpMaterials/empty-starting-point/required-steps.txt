* Install npm: https://www.npmjs.com/
* Install gulp: npm install gulp -g
* Download gulp dependencies: npm install
* Create gulpfile.js, this could be considered as your 'main method' for gulp
* Determine which gulp plugins you would like to use and add them to your package.json
* Install plugins: npm install

