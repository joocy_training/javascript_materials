/**
 * This could be considered our start file for gulp, it is the file that the 'gulp' command will look for when run.
 *
 * You should avoid doing much in this file except load the other gulp files in your project as this 
 * allows for gulp files to easily be reused between projects.
 */

/**
 * Initalise our dependencies
 */
var gulp = require('gulp'),
    wrench = require('wrench'),
    del = require('del');

/**
 * This will load all your gulp files in the gulp-files directory
 * in order to load all gulp tasks defined.
 *
 * Gulp will read both standard javascript and coffeescript files.
 */
wrench.readdirSyncRecursive('./gulp-files')
  .filter(function(file) {
    return (/\.(js|coffee)$/i).test(file);
  })
  .map(function(file) {
    require('./gulp-files/' + file);
  });

/**
 * The default task, consider this your 'main method'.
 */
gulp.task('default', ['build']);

/**
 * Here we are building the minified styles, scripts, images, and html tasks when gulp is run
 */
gulp.task('build', ['clean', 'styles', 'scripts', 'images', 'build-html']);

/**
 * Cleans all files in the dist folder via the use of the del plugin.
 */
gulp.task('clean', function() {
  return del(['dist', 'tmp']);
});
