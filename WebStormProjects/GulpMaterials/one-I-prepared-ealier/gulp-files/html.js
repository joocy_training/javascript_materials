/**
 * We inject our dependencies into our index.html file from bower and our local files.
 * We can output both a minified version and a 'debug' version for debugging.
 */

 /**
  * Initalise our dependencies
  */
var gulp = require('gulp'),
    wiredep = require('wiredep').stream,
    inject = require('gulp-inject'),
    rename = require('gulp-rename'),
    _ = require('lodash'),
    notify = require('gulp-notify');

/**
 * The task 'build-html' is registered with gulp and can be manually trigged by running 'gulp build-html' from your command line.
 * This task will call both the 'build-big-html' and 'build-minified-html' tasks.
 */
gulp.task('build-html', ['build-debug-html', 'build-minified-html']);

/**
 * The task 'build-debug-html' is registered with gulp and can be manually trigged by running 'gulp build-debug-html' from your command line.
 */
gulp.task('build-debug-html', function () {
  return gulp.src('./src/index.html') // Create unminified html
    .pipe(rename({suffix: '.debug'})) // Add .debug suffix.
    .pipe(injectJavascript([".js", "!.min.js"])) // Inject unminfied javascript files, using the bang symbol to ignore any of those suffexes. 
    .pipe(injectStyles([".css", "!.min.css"])) // Inject unminfied css files. 
    .pipe(wiredep({ // Inject bower dependencies.
        exclude: [/bootstrap.js$/, /bootstrap\.css/],
        directory: 'bower_components'
    }))
    .pipe(gulp.dest('./dist')) // Save to dist with the filename 'index.debug.html'.
    .pipe(notify({message: 'Scripts and styles injected succesfully'}));
})

/**
 * The task 'build-minified-html' is registered with gulp and can be manually trigged by running 'gulp build-minified-html' from your command line.
 */
gulp.task('build-minified-html', function () {
  return gulp.src('./src/index.html') // Created a minified html.
    .pipe(injectJavascript(".min.js")) // Inject minified javascript files.
    .pipe(injectStyles(".min.css")) // Inject minified css files.
    .pipe(wiredep({ // Inject bower dependencies.
        exclude: [/bootstrap.js$/, /bootstrap\.css/],
        directory: 'bower_components'
    }))
    .pipe(gulp.dest('./dist')) // Save to dist with the filename 'index.html'.
    .pipe(notify({message: 'Minified scripts and styles injected succesfully'}));
})

/**
 * Function to help build our injector syntax for javascript files.
 */
function injectJavascript(suffixes) {
  return inject( // Inject will auto detect that these are javascript files and inject under the <!-- inject:js --> tag.
    gulp.src(addSuffixesTo('dist/scripts/**/*', suffixes), { read: false }), { // Lists all js files from the dist/scripts directory.
      addRootSlash: false,
      transform: function(filePath) { // This function will transform the file path into the below script tag
        return '<script src="' + filePath.replace('dist/', '') + '"></script>';
      }
    }
  )
}

/**
 * Function to help build our injector syntax for css files.
 */
function injectStyles(suffixes) {
  return inject( // Inject will auto detect that these are css files and inject under the <!-- inject:css --> tag.
    gulp.src(addSuffixesTo('dist/styles/**/*', suffixes), { read: false }), { // Lists all js files from the dist/styles directory.
      addRootSlash: false,
      transform: function(filePath) { // This function will transform the file path into the below style tag
        return '<link rel="stylesheet" href="' + filePath.replace('dist/', '') + '"/>';
      }
    }
  );
}

/**
 * Adds an array of suffixes to the provided path.
 */
function addSuffixesTo(path, suffexes) {
  if (_.isString(suffexes)) { // Add single suffex string to a one element array.
    suffexes = [suffexes];
  }

  return _.map(suffexes, function (suffex) { // Add path to each suffex.
    if (suffex.includes('!')) {
      return "!" + path + suffex.replace("!", ""); // Add bang to tell gulp to ignore these files/
    }
    return path + suffex; // Don't add bang to include these files.
  });
}