/**
 * We watch for any changes made to any of the files in our src folder, and reload the web browser if any changes occur.
 */

/**
 * Initalise our dependencies
 */
var gulp = require('gulp'),
    livereload = require('gulp-livereload');

/**
 * The task 'watch' is registered with gulp and can be manually trigged by running 'gulp watch' from your command line.
 */
gulp.task('watch', ['build'], function() {
  gulp.watch(['src/**/*.css', 'src/**/*.scss'], ['styles']); // Watch css files and run the task 'styles' if any changes detected
  gulp.watch('src/scripts/**/*.js', ['scripts']); // Watch js files and run the task 'scripts' if any changes detected
  gulp.watch('src/images/**/*', ['images']); // Watch image files and run the task 'images' if any changes detected
  gulp.watch('src/*.html', ['build-html']); // Watch html files and run the task 'bower' if any changes detected

  livereload.listen(); // Create LiveReload server for the chrome live reload plugin

  gulp.watch(['dist/**']).on('change', livereload.changed); // Watch any files in dist/, reload browser on change
});