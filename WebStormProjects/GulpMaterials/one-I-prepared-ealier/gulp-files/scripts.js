/**
 * We run jshint on all javascript files from a jshintrc file found in the root directory, 
 * and if it passes we concatenate all our javascript files into one 'main.js' file and minify it.
 */

/**
 * Initalise our dependencies
 */
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify');

/**
 * The task 'scripts' is registered with gulp and can be manually trigged by running 'gulp scripts' from your command line.
 */
gulp.task('scripts', function() {
  return gulp.src('src/scripts/**/*.js') // Select all files found in src/scripts and which end with '.js'.
    .pipe(jshint()) // Run JS hint.
    .pipe(jshint.reporter('default')) // Will output the report and fail if there are any issues.
    .pipe(concat('main.js')) // Concatentate all our javascript files into one 'main.js' file.
    .pipe(gulp.dest('dist/scripts')) // Save this file to dist/scripts.
    .pipe(rename({ suffix: '.min' })) // Add .min suffix, but keeping the original 'main.js' file intact.
    .pipe(uglify()) // Minify the code.
    .pipe(gulp.dest('dist/scripts')) // Save a minified 'main.min.js' file.
    .pipe(notify({ message: 'Scripts task complete' }));
});