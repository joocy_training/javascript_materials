/**
 * We add add use autoprefixer(https://github.com/postcss/autoprefixer) to automaticly add vendor prefixs for our css 
 * classes and types, and then concatenate and minfy our css.
 *
 * This script could be extended to support SASS and LESS processing.
 */

/**
 * Initalise our dependencies
 */
var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass');

/**
 * The task 'styles' is registered with gulp and can be manually trigged by running 'gulp styles' from your command line.
 */
gulp.task('styles', ['sass'], function() { // Run the sass task as a depdancy.
  return gulp.src(['./src/**/*.css', './tmp/sass/*.css']) // Select all files found in 'src' and 'tmp' directories which end with '.css'.
    .pipe(concat('main.css')) // Concatentate all our css files into one 'main.css' file.
    .pipe(autoprefixer('last 2 version')) // Adds vendor specific prefixs for classes and types. Refer to https://github.com/postcss/autoprefixer
    .pipe(gulp.dest('dist/styles')) // Save file to dist/styles.
    .pipe(rename({ suffix: '.min' })) // Add .min suffix, but keeping the original 'main.css' file intact.
    .pipe(cssnano()) // Minify the styles file.
    .pipe(gulp.dest('dist/styles')) // Save minified file 'main.min.css' to dist/styles.
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('sass', function() {
  return gulp.src(['./src/**/*.scss', '!./src/**/_*.scss']) // Select all files found in 'src' that end with '.sass', but do not start with an underscore (e.g. '_main.scss').
    .pipe(sass().on('error', sass.logError)) // Compile sass files to css.
    .pipe(gulp.dest('tmp/sass')) // Save files to tmp/sass.
});