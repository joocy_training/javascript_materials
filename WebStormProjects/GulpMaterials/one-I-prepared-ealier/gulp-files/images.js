/**
 * We minify our images here, and cache them so that we do not have to minfiy for each change of source.
 * This only affects images in the 'dist' folder
 */

/**
 * Initalise our dependencies
 */
var gulp = require('gulp'),
    cache = require('gulp-cache'),
    imagemin = require('gulp-imagemin'),
    notify = require('gulp-notify');

/**
 * The task 'images' is registered with gulp and can be manually trigged by running 'gulp images' from your command line.
 */
gulp.task('images', function() {
  return gulp.src('src/images/**/*') // Select all files found in src/images, and then for each file do below
    .pipe(cache( // Caches the minified image so that it will not have to be reloaded everytime there is a change
    	imagemin({ // Minifies the current image
    		optimizationLevel: 3, 
    		progressive: true, 
    		interlaced: true
		})
	))
    .pipe(gulp.dest('dist/images')) // Saves all images to dist/images
    .pipe(notify({message: 'Images task complete'})); // Only print message once all images are complete
});