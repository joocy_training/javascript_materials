import restapi from './restapi';
import {log} from '../../logger';

function error() {
    log('Error fetching data');
}

function handleReply(data) {
    log('Replied with: ' + data.message);
}

let api = restapi().forBaseUrl('http://localhost:8080/api');

api.get('/hello').execute()
    .then(handleReply)
    .catch(error);

restapi().addDefaultHeader('Auth', 'user1:password1');

api.get('/hello').execute()
    .then(handleReply)
    .catch(error);

api.post('/mynameis/EamonnBoyle').execute()
    .then(handleReply)
    .catch(error);

api.put('/howdy')
    .addHeader('firstname', 'Eamonn')
    .addHeader('lastname', 'Boyle')
    .execute()
    .then(handleReply)
    .catch(error);

api.delete('/worries')
    .setBody([
        'Nuclear Winter',
        'Asteroids',
        'Pickles',
    ])
    .execute()
    .then(handleReply)
    .catch(error);
