class RestApiBuilder {
    constructor(method, resource, options) {
        this.resource = resource;
        this.options = {
            ...options,
            method: method,
        };
    }

    addHeader(key, value) {
        this.options.headers[key] = value;
        return this;
    }

    setBody(body) {
        this.options.body = JSON.stringify(body);
        return this;
    }

    async execute() {
        const response = await fetch(this.resource, this.options);
        if (response.ok) {
            return await response.json();
        }

        throw new Error(await response.text());
    }
}

class RestApiBuilderFactory {
    constructor(base = '') {
        this.base = base;
        this.defaultOptions = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        };
    }

    forBaseUrl(base) {
        return Object.create(this, {
            base: {value: base},
        });
    }

    get(resource) {
        return this.createBuilder('GET', resource, this.defaultOptions);
    }

    post(resource) {
        return this.createBuilder('POST', resource, this.defaultOptions);
    }

    put(resource) {
        return this.createBuilder('PUT', resource, this.defaultOptions);
    }

    delete(resource) {
        return this.createBuilder('DELETE', resource, this.defaultOptions);
    }

    createBuilder(method, resource, options) {
        return new RestApiBuilder(method, this.base + resource, options);
    }

    addDefaultHeader(key, value) {
        this.defaultOptions.headers[key] = value;
    }
}

let instance = null;

export default () => {
    if (instance == null) {
        instance = new RestApiBuilderFactory();
    }
    return instance;
};
