import restapi from './restapi';
import {log} from '../../logger';

function handleError() {
    log('Error fetching data');
}

function handleReply(data) {
    log('Replied with: ' + data.message);
}

async function request(action) {
    try {
        const reply = await action().execute();
        handleReply(reply);
    } catch (msg) {
        handleError(msg);
    }
}

(async () => {
    let api = restapi().forBaseUrl('http://localhost:8080/api');

    request(() => api.get('/hello'));

    restapi().addDefaultHeader('Auth', 'user1:password1');

    request(() => api.get('/hello'));

    request(() => api.post('/mynameis/EamonnBoyle'));
    request(() => api.put('/howdy')
        .addHeader('firstname', 'Eamonn')
        .addHeader('lastname', 'Boyle')
    );

    request(() => api.delete('/worries')
        .setBody([
            'Nuclear Winter',
            'Asteroids',
            'Pickles',
        ])
    );
})();
