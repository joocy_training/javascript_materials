
export class Event {
    constructor() {
        this.observers = [];
    }

    register(handler) {
        this.observers.push(handler);
    }

    unregister(handler) {
        this.observers =
            this.observers.filter(x => x !== handler);
    }

    emit(...params) {
        for (let observer of this.observers) {
            observer(...params);
        }
    }
}
