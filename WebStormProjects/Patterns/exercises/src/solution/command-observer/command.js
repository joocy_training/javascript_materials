import {Calculator} from './calculator';
import {UndoManager} from './undo-manager';

const resultNode = document.getElementById('result');
const valueNode = document.getElementById('value');

let addEventHandlerToNode = (nodeName, handler) => {
    document.getElementById(nodeName).addEventListener(
        'click',
        handler
    );
};

let calculator = new Calculator();
let undoManager = new UndoManager();

let updateResult = (x) => {
    resultNode.innerHTML = x;
};

let getValue = () => {
    return parseFloat(valueNode.value);
};

addEventHandlerToNode('add', () => {
    let value = getValue();
    calculator.add(value);
    undoManager.addUndo(() => calculator.subtract(value));
});

addEventHandlerToNode('subtract', () => {
    let value = getValue();
    calculator.subtract(value);
    undoManager.addUndo(() => calculator.add(value));
});

addEventHandlerToNode('multiply', () => {
    let value = getValue();
    calculator.multiply(value);
    undoManager.addUndo(() => calculator.divide(value));
});

addEventHandlerToNode('divide', () => {
    let value = getValue();
    calculator.divide(value);
    undoManager.addUndo(() => calculator.multiply(value));
});

addEventHandlerToNode('undo', () => {
    undoManager.undo();
});

updateResult(calculator.value);


calculator.onchange.register(x => updateResult(x));

// Why will the following line fail?
// calculator.onchange.unregister(x => updateResult(x));