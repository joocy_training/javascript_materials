function* fibonacci() {
    let current = 1;
    let next = 1;
    while (true) {
        yield current;
        const newNext = next + current;
        current = next;
        next = newNext;
    }
}

let count = 0;
for (let number of fibonacci()) {
    if (number >= 20000000) {
        break;
    }
    count++;
    console.log(number);
}

console.log(`There are ${count} Fibonacci numbers under 20 million`);

// Solutino using general Enumeration function

function* enumerate(items) {
    let count = 0;
    for (let item of items) {
        yield [count++, item];
    }
}

for (let [count, number] of enumerate(fibonacci())) {
    if (number >= 20000000) {
        console.log(`There are ${count} Fibonacci numbers under 20 million`);
        break;
    }
}
