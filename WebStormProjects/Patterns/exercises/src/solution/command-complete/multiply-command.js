import {AddCommand} from "./add-command";

export class MultiplyCommand {
    constructor(calculator, value) {
        this._calculator = calculator;
        this._value = value;
    }

    execute() {
        this._calculator.multiply(this._value);
    }

    undo() {
        this._calculator.divide(this._value);
    }
}

MultiplyCommand.id = 'multiply';
