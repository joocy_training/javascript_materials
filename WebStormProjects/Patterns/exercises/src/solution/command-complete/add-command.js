import {registerCommand} from "./command";

export class AddCommand {
    constructor(calculator, value) {
        this._calculator = calculator;
        this._value = value;
    }

    execute() {
        this._calculator.add(this._value);
    }

    undo() {
        this._calculator.subtract(this._value);
    }
}

AddCommand.id = 'add';
