export class SubtractCommand {
    constructor(calculator, value) {
        this._calculator = calculator;
        this._value = value;
    }

    execute() {
        this._calculator.subtract(this._value);
    }

    undo() {
        this._calculator.add(this._value);
    }
}

SubtractCommand.id = 'subtract';
