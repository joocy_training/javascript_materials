import {AddCommand} from "./add-command";

export class DivideCommand {
    constructor(calculator, value) {
        this._calculator = calculator;
        this._value = value;
    }

    execute() {
        this._calculator.divide(this._value);
    }

    undo() {
        this._calculator.multiply(this._value);
    }
}

DivideCommand.id = 'divide';
