import {Event} from './event';

export class Calculator {
    constructor() {
        this.value = 34;
        this.onchange = new Event();
    }

    add(value) {
        this.value = this.value + value;
        this.notifyChange();
    }

    subtract(value) {
        this.value -= value;
        this.notifyChange();
    }

    multiply(value) {
        this.value *= value;
        this.notifyChange();
    }

    divide(value) {
        this.value /= value;
        this.notifyChange();
    }

    notifyChange() {
        this.onchange.emit(this.value);
    }
}
