import {Calculator} from './calculator';
import {UndoManager} from './undo-manager';
import {AddCommand} from './add-command';
import {SubtractCommand} from './subtract-command';
import {MultiplyCommand} from './multiply-command';
import {DivideCommand} from './divide-command';

const resultNode = document.getElementById('result');
const valueNode = document.getElementById('value');

let addEventHandlerToNode = (nodeName, handler) => {
    document.getElementById(nodeName).addEventListener(
        'click',
        handler
    );
};

let calculator = new Calculator();
let undoManager = new UndoManager();

let updateResult = (x) => {
    resultNode.innerHTML = x;
};

let getValue = () => {
    return parseFloat(valueNode.value);
};

addEventHandlerToNode('undo', () => {
    undoManager.undo();
});


const commands = [
    {name: 'add', factory: () => new AddCommand(calculator, getValue())},
    {name: 'subtract', factory: () => new SubtractCommand(calculator, getValue())},
    {name: 'multiply', factory: () => new MultiplyCommand(calculator, getValue())},
    {name: 'divide', factory: () => new DivideCommand(calculator, getValue())},
];

for (let command of commands) {
    addEventHandlerToNode(command.name, () => {
        const commandObject = command.factory();
        commandObject.execute();
        undoManager.addUndo(commandObject);
    });
}

// const commands = [
//     AddCommand,
//     SubtractCommand,
//     MultiplyCommand,
//     DivideCommand
// ];

// for (let Command of commands) {
//     addEventHandlerToNode(Command.id, () => {
//         const commandObject = new Command(calculator, getValue());
//         commandObject.execute();
//         undoManager.addUndo(commandObject);
//     });
// }

updateResult(calculator.value);
calculator.onchange.register(x => updateResult(x));


// Why will the following line fail?
// calculator.onchange.unregister(x => updateResult(x));
