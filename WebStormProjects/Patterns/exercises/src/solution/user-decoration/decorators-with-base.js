import {encode, decode} from './cryptography';

class UserDecoratorBase {
    login() {
        this.user.login();
    }

    constructor(user) {
        this.user = user;
    }

    get name() {
        return this.user.name;
    }

    setFirstName(firstName) {
        this.user.setFirstName(firstName);
    }

    setLastName(lastName) {
        this.user.setLastName(lastName);
    }

    get password() {
        return this.user.password;
    }

    set password(newPassword) {
        this.user.password = newPassword;
    }
}

export class NameReverseUserDecorator extends UserDecoratorBase {
    constructor(user) {
        super(user);
    }

    get name() {
        return `${this.user._lastName}, ${this.user._firstName}`;
    }
}

export class PasswordEncoderUserDecorator extends UserDecoratorBase {
    constructor(user, shift) {
        super(user);
        this.shift = shift;
    }

    get password() {
        return decode(this.user.password, this.shift);
    }

    set password(newPassword) {
        this.user.password = encode(newPassword, this.shift);
    }
}