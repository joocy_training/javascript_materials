import {log} from '../../logger';

export function loggingProxy(method) {
    return {
        get: (target, property) => {
            if (property !== method) {
                return target[property];
            }

            return (...params) => {
                log('==== Before calling method: ' + method);
                let result = target[method](...params);
                log('==== After calling method: ' + method);
                return result;
            };
        },
    };
}

