import {User} from './user';
import {NameReverseUserDecorator} from './name-reverse-user-decorator';
import {PasswordEncoderUserDecorator} from './password-encoder-user-decorator';
import {loggingProxy} from './logging-proxy';
import {log} from '../../logger';

function outputUser(user) {
    log(`Name: ${user.name}`);
    log(`Password: ${user.password}`);
}

let user = new User('Bob', 'Bobson', 'tiger14');
let decoratedUser = new Proxy(new PasswordEncoderUserDecorator(
                                new NameReverseUserDecorator(user),
                                12),
                              loggingProxy('login'));

outputUser(user);
outputUser(decoratedUser);

decoratedUser.setFirstName('Robert');
decoratedUser.password = '16JollyRavers';
decoratedUser.login();

log('-------');

outputUser(user);
outputUser(decoratedUser);
