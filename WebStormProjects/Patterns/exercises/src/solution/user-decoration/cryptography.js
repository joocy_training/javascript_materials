const characters = 'abcdefghijklmnopqrstuvwxyz 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const characterCount = characters.length;

export function encode(data, shift) {
    let chars = data. split('');
    let encodedChars = chars.map(x => {
        let index = characters.indexOf(x);
        if (index === -1) {
            return x;
        }
        return characters[(index + shift + characterCount) % characterCount];
    });
    return encodedChars.join('');
}

export function decode(data, shift) {
    return encode(data, -shift);
}