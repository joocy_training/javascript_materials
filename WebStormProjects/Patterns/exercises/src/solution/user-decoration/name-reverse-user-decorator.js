export class NameReverseUserDecorator {
    constructor(user) {
        this.user = user;
    }

    get name() {
        // Why is this dangerous
        return `${this.user._lastName}, ${this.user._firstName}`;
    }

    setFirstName(firstName) {
        this.user.setFirstName(firstName);
    }

    setLastName(lastName) {
        this.user.setLastName(lastName);
    }

    get password() {
        return this.user.password;
    }

    set password(newPassword) {
        this.user.password = newPassword;
    }

    login() {
        this.user.login();
    }
}