import {encode, decode} from './cryptography';

export class PasswordEncoderUserDecorator {
    constructor(user, shift) {
        this.user = user;
        this.shift = shift;
    }

    get name() {
        return this.user.name;
    }

    setFirstName(firstName) {
        this.user.setFirstName(firstName);
    }

    setLastName(lastName) {
        this.user.setLastName(lastName);
    }

    get password() {
        return decode(this.user.password, this.shift);
    }

    set password(newPassword) {
        this.user.password = encode(newPassword, this.shift);
    }

    login() {
        this.user.login();
    }
}
