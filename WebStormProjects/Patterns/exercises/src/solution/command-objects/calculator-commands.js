class AddCommand {
    constructor(calculator, value) {
        this.calculator = calculator;
        this.value = value;
    }

    invoke() {
        this.calculator.add(this.value);
    }

    undo() {
        this.calculator.subtract(this.value);
    }
}

class MultiplyCommand {
    constructor(calculator, value) {
        this.calculator = calculator;
        this.value = value;
    }

    invoke() {
        this.calculator.multiply(this.value);
    }

    undo() {
        this.calculator.divide(this.value);
    }
}

class DivideCommand {
    constructor(calculator, value) {
        this.calculator = calculator;
        this.value = value;
    }

    invoke() {
        this.calculator.divide(this.value);
    }

    undo() {
        this.calculator.multiply(this.value);
    }
}

export class CalculatorCommandFactory {
    constructor(calculator) {
        this.calculator = calculator;
    }

    create(name, value) {
        if (name === 'add') {
            return new AddCommand(this.calculator, value);
        }
        if (name === 'subtract') {
            return new AddCommand(this.calculator, -value);
        }
        if (name === 'multiply') {
            return new MultiplyCommand(this.calculator, value);
        }
        if (name === 'divide') {
            return new DivideCommand(this.calculator, value);
        }
        throw Error('Unknown calculator command');
    }
}