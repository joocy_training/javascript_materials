import {Calculator} from './calculator';
import {UndoManager} from './undo-manager';
import {CalculatorCommandFactory} from './calculator-commands';

const resultNode = document.getElementById('result');
const valueNode = document.getElementById('value');

let calculator = new Calculator();
const calculatorFactory = new CalculatorCommandFactory(calculator);

let undoManager = new UndoManager();
let updateResult = () => {
    resultNode.innerHTML = calculator.value;
};

let getValue = () => {
    return parseFloat(valueNode.value);
};

let addEventHandlerToNode = (name, handler) => {
    document.getElementById(name).addEventListener(
        'click',
        handler
    );
};

let addCalculationOperation = (name) => {
    function createOperationHandler() {
        let command = calculatorFactory.create(name, getValue());
        command.invoke();
        undoManager.addUndo(command);
        updateResult();
    }

    addEventHandlerToNode(name, createOperationHandler);
};

addCalculationOperation('add', );
addCalculationOperation('subtract');
addCalculationOperation('multiply');
addCalculationOperation('divide');

addEventHandlerToNode('undo', () => {
    undoManager.undo();
    updateResult();
});

updateResult();
