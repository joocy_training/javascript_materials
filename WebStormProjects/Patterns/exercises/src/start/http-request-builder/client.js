import {log} from '../../logger';

// **************
// *** Rework to simplify with builder pattern
// *** Use a singleton to expose the builder and encapsulate inner types
// **************
function error(msg) {
    log('Error fetching data - ' + msg);
}

function mapError(response) {
    if (response.ok) {
        return response;
    } else {
        return response.text().then(msg => {
            throw new Error(msg);
        });
    }
}

const base = 'http://localhost:8080/api';

// Failed Authorisation
fetch(base + '/hello')
    .then(mapError)
    .then((res) => res.json())
    .then((data) => {
        log('Replied with: ' + data.message);
    })
    .catch(error);

// Getting hello from server
fetch(base + '/hello', {headers: {'Auth': 'user1:password1'}})
    .then((res) => res.json())
    .then((data) => {
        log('Replied with: ' + data.message);
    })
    .catch(error);

// Posting my name
fetch(base + '/mynameis/EamonnBoyle', {method: 'POST', headers: {'Auth': 'user1:password1'}})
    .then(mapError)
    .then((res) => res.json())
    .then((data) => {
        log('Replied with: ' + data.message);
    })
    .catch(error);

// Putting my name
fetch(base + '/howdy', {
    method: 'PUT',
    headers: {
        'Auth': 'user1:password1',
        'Content-Type': 'application/json',
        'firstname': 'Eamonn',
        'lastname': 'Boyle',
    }})
    .then(mapError)
    .then((res) => res.json())
    .then((data) => {
        log('Replied with: ' + data.message);
    })
    .catch(error);

// Deleting my worries
fetch(base + '/worries', {
    method: 'DELETE',
    headers: {
        'Auth': 'user1:password1',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify([
        'Nuclear Winter',
        'Asteroids',
        'Pickles',
    ])})
    .then(mapError)
    .then((res) => res.json())
    .then((data) => {
        log('Replied with: ' + data.message);
    })
    .catch(error);
