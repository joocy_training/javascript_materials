
export function log(message) {
    const pre = document.createElement('pre');
    pre.appendChild(document.createTextNode(message));

    document.body.appendChild(pre);
}
