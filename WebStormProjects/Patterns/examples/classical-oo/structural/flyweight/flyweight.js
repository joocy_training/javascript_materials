import {TextBox} from './textbox';
import {TextBoxShared} from './textbox-shared';
import {log} from '../../logger';

const boxCount = 10000;

let boxes = [];
log(`Creating ${boxCount} text boxes`);
for (let ii = 0; ii < boxCount; ii++) {
    let textBox = new TextBox();
    textBox.width = ii / 2;
    boxes.push(textBox);
}

log('Finished creating boxes');
log(`TextBox[123] has border color ${boxes[123].bordercolor}`);
log(`TextBox[123] has width ${boxes[123].width}`);


let boxesShared = [];
log(`Creating ${boxCount} shared text boxes`);
for (let ii = 0; ii < boxCount; ii++) {
    let textBox = new TextBoxShared();
    textBox.width = ii / 2;
    boxesShared.push(textBox);
}

log('Finished creating boxes');
log(`TextBox[123] has border color ${boxesShared[123].bordercolor}`);
log(`TextBox[123] has width ${boxesShared[123].width}`);
