import {log} from '../../logger';


class List {
    constructor(items = []) {
        this._items = items;
    }

    add(...items) {
        items.forEach(x => this._items.push(x));
    }

    values() {
        return this._items;
    }
}

class FilteredList {
    constructor(list, predicate) {
        this._list = list;
        this._predicate = predicate;
    }

    add(item) {
        this._list.add(item);
    }

    values() {
        return this._list.values().filter(this._predicate);
    }
}


let data = new List([12, 34, 45, 67, 4, 9]);
let evenData = new FilteredList(data, x => x % 2 === 0);

function outputArray(list) {
    let values = list.values();
    for (let index in values) {
        log(`  array[${index}] = ${values[index]}`);
    }
    // list.forEach((item, index) => log(`  list[${index}] = ${item}`));
}

log(`Filtered Data:`);
outputArray(evenData);
data.add(13);
data.add(32);
log(`After adding to source:`);
outputArray(evenData);
