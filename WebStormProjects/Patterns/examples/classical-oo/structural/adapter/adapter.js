import {MathLibrary} from './math-library';
import {MathLibraryAdapter} from './math-library-adapter';
import {log} from '../../logger';


let localMathInterface = new MathLibraryAdapter(
    new MathLibrary()
);
log(`2 + 5 = ${localMathInterface.sum(2, 5)}`);
log(`12 - 8 = ${localMathInterface.difference(12, 8)}`);
log(`Pi = ${localMathInterface.getPi()}`);
log(`4 * 7 = ${localMathInterface.product(4, 7)}`);
