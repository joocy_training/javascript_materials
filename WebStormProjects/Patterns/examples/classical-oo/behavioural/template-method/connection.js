export class Connection {
    sendMessage(data) {
        this._openConnection();
        this._sendData(data);
        this._closeConnection();
    }
}

