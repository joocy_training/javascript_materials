import {log} from '../../logger';
import {Connection} from './connection';

export class HttpConnection extends Connection {
    _openConnection() {
        log('HTTP opening connection');
    }

    _sendData(data) {
        log('HTTP sending: ' + data);
    }

    _closeConnection() {
        log('HTTP opening connection');
    }
}
