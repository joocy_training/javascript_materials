import {log} from '../../logger';
import {Connection} from './connection';

export class SmtpConnection extends Connection {
    _openConnection() {
        log('SMTP opening connection');
    }

    _sendData(data) {
        log('SMTP sending: ' + data);
    }

    _closeConnection() {
        log('SMTP opening connection');
    }
}
