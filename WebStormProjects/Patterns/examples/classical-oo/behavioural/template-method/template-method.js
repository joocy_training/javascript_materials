import {HttpConnection} from './http-connection';
import {SmtpConnection} from './smtp-connection';

const httpConnection = new HttpConnection();
const smtpConnection = new SmtpConnection();

httpConnection.sendMessage('Sending first message');
smtpConnection.sendMessage('Sending second message');
