import {log} from '../../logger';

function dumpEmployee(employee) {
    log(JSON.stringify(employee));
}

log('----');
log('Freeze Example');

const employee = {
    name: 'Charlie',
};

dumpEmployee(employee);

log('----');
log('Modifying object');
employee.name = 'Stevie';
employee.age = 21;
dumpEmployee(employee);

Object.freeze(employee);

try {
    log('----');
    log('Attempting modification after freezing');

    employee.name = 'Tara';
    dumpEmployee(employee);
}
catch (error) {
    log('Exception thrown - ' + error);
    dumpEmployee(employee);
}

try {
    log('----');
    log('Attempting adding properties after freezing');

    employee.salary = 200000;
    dumpEmployee(employee);
}
catch (error) {
    log('Exception thrown - ' + error);
    dumpEmployee(employee);
}
