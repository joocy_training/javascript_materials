import {log} from '../../logger';

export class Cheque {
    credit(amount) {
        log(`Paying £${amount} via cheque`);
    }
}

export class BankTransfer {
    credit(amount) {
        log(`Paying £${amount} via bank transfer`);
    }
}
