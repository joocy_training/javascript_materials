import {authenticationServiceInstance} from './authentication-service';
import {ConfigurationLoader} from './configuration-loader';
import {log} from '../../logger';


new ConfigurationLoader().load();
log(`Is Logged in ${authenticationServiceInstance().isLoggedIn}`);

