import {log} from '../../logger';

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

export class Square {
    draw() {
        log(`Drawing square as (${this.position.x}, ${this.position.y})`);
        ctx.beginPath();
        ctx.fillStyle = 'red';
        ctx.fillRect(this.position.x, this.position.y, 40, 40);
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.strokeRect(this.position.x, this.position.y, 40, 40);
    }
}

export class Circle {
    draw() {
        log(`Drawing circle as (${this.position.x}, ${this.position.y})`);

        ctx.beginPath();
        ctx.fillStyle = 'green';
        ctx.arc(this.position.x, this.position.y, 20, 0, 2 * Math.PI);
        ctx.fill();
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.stroke();
    }
}
