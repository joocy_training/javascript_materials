import {CircleFactory, SquareFactory} from './shape-factories';
import {log} from '../../logger';

// Creating them all first to show distinct state
let shapes = [
    SquareFactory.create({x: 40, y: 60}),
    SquareFactory.create({x: 190, y: 30}),
    CircleFactory.create({x: 100, y: 180}),
    CircleFactory.create({x: 200, y: 234}),
];

shapes.forEach(x => x.draw());

