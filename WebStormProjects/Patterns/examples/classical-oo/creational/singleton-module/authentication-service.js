import {log} from '../../logger';

class AuthenticationService {
    constructor() {
        log('Creating Authentication Service');
        this.isLoggedIn = false;
    }

    login() {
        this.isLoggedIn = true;
    }

    logout() {
        this.isLoggedIn = false;
    }
}

export default new AuthenticationService();
