import authenticationService from './authentication-service';
import {log} from '../../logger';

export class ConfigurationLoader {
    load() {
        log('Loading configuration and logging in');
        log(`Currently ${authenticationService.isLoggedIn}`);
        // ...
        authenticationService.login();
    }
}
