import {log} from '../../logger';

class Rectangle {
    constructor(width, height, children) {
        this.width = width;
        this.height = height;
        this.children = children;
    }

    area() {
        return this.width * this.height;
    }

    clone() {
        return new Rectangle(this.width, this.height, [...this.children]);
    }

    toString() {
        return `Rect = [ ${this.width}, ${this.height}} ]`;
    }
}

function describe(title, rectangle) {
    log(title);
    log('----');
    try {
        log(JSON.stringify(rectangle));
        log(`Is it a rectangle? ${rectangle instanceof Rectangle}`);
        log(rectangle);
        log(`It has area ${rectangle.area()}`);
        log(`It contains ${rectangle.children.length} children`);
    } catch (error) {
        log('Error - ' + error);
    }
    log(' ');
}

const rect1 = new Rectangle(10, 12, [new Rectangle(0, 0), new Rectangle(1, 2)]);
const rect2 = rect1.clone();
const rect3 = Object.assign({}, rect1);
const rect4 = {...rect1};
const rect5 = _.clone(rect1);
const rect6 = _.cloneDeep(rect1);

rect1.width = 100;
rect1.height = 120;
rect1.children.push(new Rectangle(2, 2));

describe('Original', rect1);
describe('Custom Clone', rect2);
describe('Object assign', rect3);
describe('Spread Operator', rect4);
describe('Lodash Shallow Clone', rect5);
describe('Lodash Deep Clone', rect6);
