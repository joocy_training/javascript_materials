import {isLetter} from "./utility";

const MAX_LIVES = 6;

export class Hangman {
    constructor(movie) {
        this.movie = movie;
        this._lives = MAX_LIVES;
        this.guesses = [];
        this.movieWithGuesses = movie.split('');
        this.solutionArray = movie.toLowerCase().split('');
        this.blankLetters();
    }

    blankLetters() {
        for (let ii = 0; ii < this.movieWithGuesses.length; ii++) {
            if (isLetter(this.movieWithGuesses[ii])) {
                this.movieWithGuesses[ii] = '_';
            }
        }
    }

    get lives() {
        return this._lives;
    }

    get isSolved() {
        return this.movie === this.movieWithGuesses.join("");
    }

    get isDead() {
        return this.lives === 0;
    }

    makeGuess(letter) {
        if (this.isSolved || this.isDead  || this.guesses.indexOf(letter) >= 0) {
            return;
        }

        if (!this.applyGuess(letter)) {
            this._lives--;
        }
    }

    applyGuess(guess) {
        guess = guess.toLowerCase();
        this.guesses.push(guess);
        this.guesses.sort();

        return this.updateMovieSolution(guess);
    }

    updateMovieSolution(guess) {
        let guessCorrect = false;
        for (let ii = 0; ii < this.movieWithGuesses.length; ii++) {
            if (this.solutionArray[ii] === guess) {
                this.movieWithGuesses[ii] = this.movie.charAt(ii);
                guessCorrect = true;
            }
        }
        return guessCorrect;
    }

    getGuesses() {
        return `[${this.guesses.join()}]`;
    }

    getSolution() {
        return this.movieWithGuesses.map(c => c === ' ' ? " /  " : c + " ").join('');
    }
}