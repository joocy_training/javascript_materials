const successMessage = "Well done!";
const deathMessage = "You're Dead!!!";

export class HangmanView {
    constructor(name) {
        this.canvas = document.getElementById(name);
        this.ctx = this.canvas.getContext("2d");
        this.lives = 6;
        this.isSolved = false;
    }

    setLives(lives) {
        this.lives = lives;
    }

    setSolved(isSolved) {
        this.isSolved = isSolved;
    }

    redraw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawGallows();

        if (this.lives < 6) {
            this.drawHead();
        }

        if (this.lives < 5) {
            this.drawTorso();
        }

        if (this.lives < 4) {
            this.drawLeftLeg();
        }

        if (this.lives < 3) {
            this.drawRightLeg();
        }

        if (this.lives < 2) {
            this.drawLeftArm();
        }

        if (this.lives < 1) {
            this.drawRightArm();
            this.drawDeath();
        }
        if (this.isSolved) {
            this.drawSuccess();
        }
    }

    drawHead() {
        this.ctx.beginPath();
        this.ctx.lineWidth = 2;
        this.ctx.fillStyle = "yellow";
        this.ctx.strokeStyle = "black";
        this.ctx.arc(150, 70, 20, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.stroke();

        this.drawEyes();
        this.drawMouth();
    }

    drawEyes() {
        this.ctx.beginPath();
        this.ctx.lineWidth = 0.5
        this.ctx.fillStyle = "blue";
        this.ctx.arc(144, 60, 4, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.arc(156, 60, 4, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.stroke();
    }

    drawMouth() {
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1;
        this.ctx.beginPath();
        this.ctx.moveTo(140, 75);
        this.ctx.lineTo(160, 75);
        this.ctx.stroke();
    }

    drawGallows() {
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 3;
        this.ctx.beginPath();
        this.ctx.moveTo(150, 180);
        this.ctx.lineTo(20, 180);
        this.ctx.lineTo(20, 20);
        this.ctx.lineTo(150, 20);
        this.ctx.stroke();

        this.ctx.lineWidth = 1;
        this.ctx.lineTo(150, 50);
        this.ctx.stroke();
    }

    drawTorso() {
        this.ctx.beginPath();
        this.ctx.moveTo(150, 90);
        this.ctx.lineTo(150, 130);
        this.ctx.stroke();
    }

    drawLeftLeg() {
        this.ctx.beginPath();
        this.ctx.moveTo(150, 130);
        this.ctx.lineTo(130, 160);
        this.ctx.stroke();
    }

    drawRightLeg() {
        this.ctx.beginPath();
        this.ctx.moveTo(150, 130);
        this.ctx.lineTo(170, 160);
        this.ctx.stroke();
    }

    drawLeftArm() {
        this.ctx.beginPath();
        this.ctx.moveTo(150, 100);
        this.ctx.lineTo(130, 100);
        this.ctx.stroke();
    }

    drawRightArm() {
        this.ctx.beginPath();
        this.ctx.moveTo(150, 100);
        this.ctx.lineTo(170, 100);
        this.ctx.stroke();
    }

    drawDeath() {
        this.ctx.textAlign = "center";

        this.ctx.lineWidth = 1;
        this.ctx.font = "25px Arial";
        this.ctx.fillStyle = "red";
        this.ctx.strokeStyle = "black";
        this.ctx.strokeText(deathMessage, 100, 100);
        this.ctx.fillText(deathMessage, 100, 100);
    }

    drawSuccess() {
        this.ctx.textAlign = "center";
        this.ctx.lineWidth = 1;
        this.ctx.font = "25px Arial";
        this.ctx.fillStyle = "green";
        this.ctx.strokeStyle = "black";
        this.ctx.strokeText(successMessage, 100, 100);
        this.ctx.fillText(successMessage, 100, 100);
    }
}