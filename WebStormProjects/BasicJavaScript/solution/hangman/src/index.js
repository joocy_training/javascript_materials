import {Hangman} from "./hangman";
import {isLetter} from "./utility";
import {getRandomMovie} from "./movies";
import {HangmanView} from "./hangmanview";
import "./index.css";

let movie = getRandomMovie();
const hangman = new Hangman(movie);
const view = new HangmanView("hangmanView");

const livesText = document.getElementById("lives");
const guessesText = document.getElementById("guesses");
const progressText = document.getElementById("progress");

function updateGameState() {
    livesText.innerHTML = hangman.lives;
    guessesText.innerHTML = hangman.getGuesses();

    if (hangman.isSolved || hangman.isDead) {
        progressText.innerHTML = movie;
    } else {
        progressText.innerHTML = hangman.getSolution();
    }

    view.setLives(hangman.lives);
    view.setSolved(hangman.isSolved);
    view.redraw();
}

updateGameState();
document.onkeydown = userInput;

function userInput(e) {
    const letter = e.key.toLowerCase();

    if (isLetter(letter) && letter.length === 1) {
        hangman.makeGuess(letter);
        updateGameState();
    }
}