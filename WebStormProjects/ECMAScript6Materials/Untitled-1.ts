const fetch_joke = () => {
    return new Promise((resolve, reject) => {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
            if (xhttp.readyState == 4) {
                if (xhttp.status == 200) {
                    const response = xhttp.responseText;
                    const joke = JSON.parse(response);
                    resolve(joke.value);
                } else {
                    reject(`Error while fetching joke (${xhttp.status})`);
                }
            }
        }
        xhttp.open('GET', 'https://api.chucknorris.io/jokes/random');
        xhttp.send();
    });
}

async function run() {
    let joke = await fetch_joke();
    console.log(joke);
}

run();