import app from "./app";
import http from "http";
import {install} from "source-map-support";
import config from "./config";
import debug from "debug";

const logger = debug("serve");

install();

const server = http.createServer(app);

server.listen(process.env.PORT || config.port, () => {
    logger(`Started on port ${server.address().port}`);
});